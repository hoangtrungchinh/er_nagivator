source 'https://rubygems.org'
# Use default ruby version
ruby '2.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.5'

# Use Devise for authentication
gem 'devise'

# TODO:
# http://railscasts.com/episodes/353-oauth-with-doorkeeper?view=asciicast
# Oauth provider for your Rails app
# gem 'doorkeeper'

# Use postgresql as the database for Active Record
gem 'pg'

# Servers
gem 'thin'

# Logging
gem 'log4r'

# Profiling oink
gem 'oink'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# General ruby templating with json, bson, xml, plist and msgpack support.
# Read more: https://github.com/nesquena/rabl
gem 'rabl'

# Also add either `oj` or `yajl-ruby` as the JSON parser
gem 'oj'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false

  gem 'pry'
  gem 'calamum'
end

group :development,:test do
  # Better Errors replaces the standard Rails error page with a much better and more useful error page. Read more: https://github.com/charliesome/better_errors
  gem 'better_errors'
  gem 'binding_of_caller'

  # Read more: https://github.com/dejan/rails_panel
  gem 'meta_request'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'minitest'

  # Use debugger
  # gem 'debugger'
  gem 'byebug'

  # Testing rails app with RSpec
  gem 'rspec-rails', '~> 2.0'
  gem 'capybara'
  gem 'shoulda-matchers'
  gem 'factory_girl_rails'
  gem 'ffaker'

  # database_cleaner is not required, but highly recommended
  gem 'database_cleaner'

  # Testing rails app with Cucumber
  # gem 'cucumber-rails', :require => false

  # Add SimpleCov
  # Read more: https://github.com/colszowka/simplecov
  # Read more: https://github.com/fguillen/simplecov-rcov
  # gem 'simplecov', :require => false
  # gem 'simplecov-rcov', :require => false

  # Use Capistrano
  gem 'capistrano', '2.15.5'
  gem 'net-ssh', '2.7.0'
  gem 'rvm-capistrano'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'
