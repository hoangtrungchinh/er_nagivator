namespace :db do
  namespace :factory_girl do
    desc 'Load Factory Girl factories into the current environment\'s database.'
    task :load => :environment do
      FactoryGirl.registry.each do |(name, factory)|
        FactoryGirl.build name
      end
    end
  end
end