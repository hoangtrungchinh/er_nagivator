# encoding: utf-8

namespace :db do
  desc 'Erase and fill database'
  task init: :environment do
    query = <<-SQL
      TRUNCATE users CASCADE;
      ALTER SEQUENCE users_id_seq RESTART WITH 1;
    SQL
    ActiveRecord::Base.connection.execute(query)
    # Time for expire_at
    EXPIRE_TIME = 3.months
    date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
    expire_at = (DateTime.now.in_time_zone + EXPIRE_TIME).strftime("%Y-%m-%dT%H:%M:%S%:z")
    User.create!(
      [
        {
          email:        'khoa@ersolution.net',
          username:     'admin',
          password:     '123456789',
          fullname:     'Nguyễn Đăng Khoa',
          access_token: '23f4b0550d33b7d733a762cda04c69fce0d00b7c',
          expire_at:    expire_at,
          created_at:   date_time_now_with_time_zone,
          updated_at:   date_time_now_with_time_zone
        },
        {
          email:        'hien@ersolution.net',
          username:     'hien',
          password:     '123456789',
          fullname:     'Lưu Thế Hiển',
          access_token: '76e03840ec007606d4fce8affaa589111462a62c',
          expire_at:    expire_at,
          created_at:   date_time_now_with_time_zone,
          updated_at:   date_time_now_with_time_zone
        },
        {
          email:        'chinh@ersolution.net',
          username:     'chinh',
          password:     '123456789',
          fullname:     'Hoàng Trung Chính',
          access_token: '946793e1f74dee7fd9669a3a464c786906ba80c7',
          expire_at:    expire_at,
          created_at:   date_time_now_with_time_zone,
          updated_at:   date_time_now_with_time_zone
        }
      ]
    )

    query = <<-SQL
      TRUNCATE roles CASCADE;
      ALTER SEQUENCE roles_id_seq RESTART WITH 1;
      INSERT INTO roles(name,created_at,updated_at) VALUES
        ('super_admin',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('admin',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('user',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('leader',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('member',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0));

      TRUNCATE group_types CASCADE;
      ALTER SEQUENCE group_types_id_seq RESTART WITH 1;
      INSERT INTO group_types(name,created_at,updated_at) VALUES
        ('OPEN_GROUP',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('CLOSE_GROUP',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0)),
        ('SECRET_GROUP',CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0));
    SQL
    ActiveRecord::Base.connection.execute(query)

    query = <<-SQL
      TRUNCATE user_role_groups CASCADE;
      ALTER SEQUENCE user_role_groups_id_seq RESTART WITH 1;

      TRUNCATE groups CASCADE;
      ALTER SEQUENCE groups_id_seq RESTART WITH 1;
      INSERT INTO groups(name,group_type_id,created_at,updated_at) VALUES
        ('group_name_01',1,CURRENT_TIMESTAMP::timestamptz(0),CURRENT_TIMESTAMP::timestamptz(0));
    SQL
    ActiveRecord::Base.connection.execute(query)
    UserRoleGroup.create!(
      [
        {
          user_id: 1,
          role_id: 1,
          created_at: date_time_now_with_time_zone,
          updated_at: date_time_now_with_time_zone
        },
        {
          user_id: 2,
          role_id: 2,
          created_at: date_time_now_with_time_zone,
          updated_at: date_time_now_with_time_zone
        },
        {
          user_id: 3,
          role_id: 3,
          created_at: date_time_now_with_time_zone,
          updated_at: date_time_now_with_time_zone
        },
        {
          user_id: 1,
          role_id: 4,
          group_id: 1,
          created_at: date_time_now_with_time_zone,
          updated_at: date_time_now_with_time_zone
        }
      ]
    )
  end
end
