# encoding: utf-8

# == Schema Information
#
# Table name: user_role_groups
#
#  id         :serial                   -- Khóa chính tự tăng
#  user_id    :integer                  NOT NULL, -- Khóa ngoại: User được gán Role, Group
#  role_id    :integer                  NOT NULL, -- Khóa ngoại: Role được gán cho User
#  group_id   :integer                  -- Khóa ngoại: Group được gán cho User
#  created_at :timestamp with time zone NOT NULL, -- Ngày giờ create Thông tin user_role_group
#  updated_at :timestamp with time zone -- Ngày giờ update Thông tin user_role_group
#
class UserRoleGroup < ActiveRecord::Base
  # Add Associations
  belongs_to :user
  belongs_to :leader, class_name: 'User', foreign_key: 'user_id'
  belongs_to :role
  belongs_to :group

  # Add Validations
  # user_id
  validates :user_id,
            :presence => true,
            :on       => :create

  # role_id
  validates :role_id,
            :presence => true,
            :on       => :create

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update

  validates_uniqueness_of :user_id, :scope => [:group_id, :role_id]

end
