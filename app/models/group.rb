# encoding: utf-8

# == Schema Information
#
# Table name: groups
#
#  id                 :serial                   -- Khóa chính tự tăng
#  name               :character varying(255)   NOT NULL -- Tên group
#  descripton         :text                     -- Mô tả group
#  encrypted_password :character varying(255)   -- Mật khẩu đã được mã hóa của group
#  group_avatar_url   :character varying(255)   -- Url ảnh đại diện của group
#  group_type_id      :integer                  -- Khóa ngoại: group_type được gán cho group
#  created_at         :timestamp with time zone NOT NULL -- Ngày giờ create Thông tin group
#  updated_at         :timestamp with time zone -- Ngày giờ update Thông tin group
#
class Group < ActiveRecord::Base
  # Add Associations
  belongs_to :group_type
  has_many :user_role_groups, dependent: :destroy
  has_one :user_role_group_leader, -> { where(role_id: 4) }, class_name: 'UserRoleGroup'
  has_many :users, :through => :user_role_groups
  has_many :invite_infos, dependent: :destroy

  # Add Validations
  # name
  validates :name,
            :presence    => true,
            :on          => :create
  validates :name,
            :length      => { maximum: 255 }

  # encrypted_password
  validates :encrypted_password,
            :length      => {maximum: 255 }

  # group_avatar_url
  validates :group_avatar_url,
            :length      => { maximum: 255 }

  # group_type_id
  validates :group_type_id,
            :presence => true,
            :on       => :create

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
