# encoding: utf-8

# == Schema Information
#
# Table name: users
#
#  id                     :serial                   NOT NULL
#  email                  :character varying(255)   NOT NULL, UNIQUE
#  encrypted_password     :character varying(255)   NOT NULL
#  reset_password_token   :character varying(255)
#  reset_password_sent_at :timestamp with time zone
#  remember_created_at    :timestamp with time zone
#  sign_in_count          :integer                  NOT NULL DEFAULT 0
#  current_sign_in_at     :timestamp with time zone
#  last_sign_in_at        :timestamp with time zone
#  current_sign_in_ip     :character varying(255)
#  last_sign_in_ip        :character varying(255)
#  created_at             :timestamp with time zone NOT NULL
#  updated_at             :timestamp with time zone
#  username               :character varying(255)   UNIQUE -- Username
#  fullname               :character varying(255)   -- Tên đầy đủ
#  cell_phone             :character varying(255)   -- Số điện thoại
#  user_avatar_url        :character varying(255)   -- Url ảnh đại diện
#  access_token           :character varying(255)   NOT NULL -- Access Token cấp cho User khi User đăng nhập
#  expire_at              :timestamp with time zone NOT NULL -- Thời gian có hiệu lực của token
#
class User < ActiveRecord::Base
  # Add Associations
  has_many :user_role_groups, dependent: :destroy
  has_many :invite_infos, dependent: :destroy
  has_many :roles, :through => :user_role_groups
  has_many :groups, :through => :user_role_groups
  has_many :groups, :through => :invite_infos
  has_many :locations

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Add Validations
  # email
  validates :email,
            :presence => true,
            :on       => :create
  validates :email,
            :uniqueness => { case_sensitive: false },
            :length     => { maximum: 255 }

  # encrypted_password
  validates :encrypted_password,
            :presence => true,
            :on       => :create
  validates :encrypted_password,
            :length     => { maximum: 255 }

  # username
  validates :username,
            :presence => true,
            :on       => :create
  validates :username,
            :uniqueness => { case_sensitive: false },
            :length     => { maximum: 255 }

  # fullname
  validates :fullname,
            :length     => { maximum: 255 }

  # cell_phone
  validates :cell_phone,
            :length     => { maximum: 255 }

  # user_avatar_url
  validates :user_avatar_url,
            :length     => { maximum: 255 }

  # access_token
  validates :access_token,
            :presence => true,
            :on       => :create
  validates :access_token,
            :length   => { maximum: 255 }

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
