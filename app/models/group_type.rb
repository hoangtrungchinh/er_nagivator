# encoding: utf-8

# == Schema Information
#
# Table name: group_types
#
#  id         :serial                   -- Khóa chính tự tăng
#  name       :character varying(255)   NOT NULL, UNIQUE -- Tên loại group: OPEN_GROUP, CLOSE_GROUP, SECRET_GROUP
#  descripton :text                     -- Mô tả loại group
#  created_at :timestamp with time zone NOT NULL -- Ngày giờ create Thông tin group_type
#  updated_at :timestamp with time zone -- Ngày giờ update Thông tin group_type
#
class GroupType < ActiveRecord::Base
  # Add Associations
  has_many :groups, dependent: :destroy

  # Add Validations
  # name
  validates :name,
            :presence   => true,
            :on         => :create
  validates :name,
            :uniqueness => { case_sensitive: false },
            :length     => { maximum: 255 }

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
