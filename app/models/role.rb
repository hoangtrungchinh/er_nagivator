# encoding: utf-8

# == Schema Information
#
# Table name: roles
#
#  id          :serial                   -- Khóa chính tự tăng
#  name        :character varying(255)   NOT NULL, UNIQUE -- Tên role: super_admin, admin, user, leader, member
#  created_at  :timestamp with time zone NOT NULL -- Ngày giờ create Thông tin role
#  updated_at  :timestamp with time zone -- Ngày giờ update Thông tin role
#
class Role < ActiveRecord::Base
  # Add Associations
  has_many :user_role_groups
  has_many :users, :through => :user_role_groups

  # Add Validations
  # name
  validates :name,
            :presence   => true,
            :on         => :create
  validates :name,
            :uniqueness => { case_sensitive: false },
            :length     => { maximum: 255 }

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
