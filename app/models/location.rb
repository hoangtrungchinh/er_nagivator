# encoding: utf-8

# == Schema Information
#
# Table name: locations
#
#  id         :serial                   -- Khóa chính tự tăng
#  user_id    :integer                  NOT NULL -- Khóa ngoại: User được gán Location
#  longitude  :double precision         NOT NULL -- Kinh độ
#  latitude   :double precision         NOT NULL -- Vĩ độ
#  usertime   :timestamp with time zone NOT NULL -- Thời gian user ghi nhận được lúc gởi
#  created_at :timestamp with time zone NOT NULL -- Ngày giờ create Thông tin location
#  updated_at :timestamp with time zone -- Ngày giờ update Thông tin location
#
class Location < ActiveRecord::Base
  # Add Associations
  belongs_to :user

  # Add Validations
  # user_id
  validates :user_id,
            :presence => true,
            :on       => :create

  # longitude
  validates :longitude,
            :presence => true,
            :on       => :create
  validates :longitude,
            :numericality => { only_float: true }

  # latitude
  validates :latitude,
            :presence => true,
            :on       => :create
  validates :latitude,
            :numericality => { only_float: true }

  # usertime
  validates :usertime,
            :presence => true,
            :on       => :create

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
