# encoding: utf-8

# == Schema Information
#
# Table name: invite_infos
#
#  id         :serial                   -- Khóa chính tự tăng
#  host_id    :integer                  NOT NULL -- Khóa ngoại: User mời
#  user_id    :integer                  NOT NULL -- Khóa ngoại: User được mời
#  group_id   :integer                  NOT NULL -- Khóa ngoại: Group được mời
#  created_at :timestamp with time zone NOT NULL -- Ngày giờ create Thông tin invite_info
#  updated_at :timestamp with time zone -- Ngày giờ update Thông tin invite_info
#
class InviteInfo < ActiveRecord::Base
  # Add Associations
  belongs_to :host, class_name: 'User', foreign_key: 'host_id'
  belongs_to :user
  belongs_to :group

  # Add Validations
  # host_id
  validates :host_id,
            :presence => true,
            :on       => :create
  validates :host_id,
            :uniqueness => {
              :scope => [
                :user_id, :group_id
              ],
              :case_sensitive => false
            }

  # user_id
  validates :user_id,
            :presence => true,
            :on       => :create

  # group_id
  validates :group_id,
            :presence => true,
            :on       => :create

  # created_at
  validates :created_at,
            :presence => true,
            :on       => :create

  # updated_at
  validates :updated_at,
            :presence => true,
            :on       => :update
end
