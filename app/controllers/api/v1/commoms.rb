# encoding: utf-8

module Api
  module V1
    module Commoms
      extend ActiveSupport::Concern

      # == Get rabl_path
      #
      # Used by BaseController, PostUsersController and PostUsersLogInController.
      def rabl_path
        @rabl_path = params[:controller] + '/' + params[:action] + '.json.rabl'
      end

    end
  end
end
