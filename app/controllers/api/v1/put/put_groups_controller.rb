# # encoding: utf-8

# module Api
#   module V1
#     module Put
#       class PutGroupsController < BaseController
#         # Path of group_avatars
#         GROUP_AVATARS_PATH = 'system/group_avatars'

#         # ==== Description
#         #
#         # Cập nhật group
#         #
#         # ==== Request
#         #
#         # * Method: PUT
#         # * URL: /api/groups(.:format)
#         # * Headers:
#         #   - Content-type: application/json
#         #   - charset=UTF-8
#         #   - Authorization: Token token="23f4b0550d33b7d733a762cda04c69fce0d00b7c"
#         #   - Accept: application/vnd.er_navigator.v1
#         # * *Example:*
#         #    PUT /api/groups.json
#         #    body:
#         #    {
#         #      "leader":{
#         #        "username":"admin"
#         #      },
#         #      "name":"group_name_01",
#         #      "password":"123456789",
#         #      "image":"nội dung file",
#         #      "descripton":"mô tả group",
#         #      "group_type":{
#         #        "id":"1"
#         #      }
#         #    }
#         #
#         # ==== Request Parameters
#         #
#         # * leader
#         #   - Description: User tạo group
#         #   - Data Type: hash
#         # * username
#         #   - Description: Username của leader
#         #   - Data Type: string
#         # * name
#         #   - Description: Tên của group
#         #   - Data Type: string
#         # * password
#         #   - Description: Mật khẩu của group
#         #   - Data Type: string
#         # * image
#         #   - Description: Nội dung file ảnh đại diện của group
#         #   - Data Type: string
#         # * descripton
#         #   - Description: Mô tả group
#         #   - Data Type: text
#         # * group_type
#         #   - Description: Loại group
#         #   - Data Type: hash
#         # * id
#         #   - Description: id của loại group
#         #   - Data Type: string
#         #
#         # ==== Response
#         #
#         # - Trả về mã 200 và Group khi cập nhật thành công
#         # * *Example:*
#         #   - Khi cập nhật thành công
#         #    Status Code: 200
#         #    Body:
#         #      {
#         #        "id": 2,
#         #        "name": "group_name_01",
#         #        "descripton": "mô tả group",
#         #        "encrypted_password": "f7c3bc1d808e04732adf679965ccc34ca7ae3441",
#         #        "group_avatar_url": "/system/group_avatars/2",
#         #        "group_type_id": 1
#         #      }
#         #
#         #   - Khi cập nhật không thành công:
#         #     - User không tồn tại trong hệ thống
#         #      Status Code: 400
#         #      Body:
#         #        {
#         #          "code":"0x0001",
#         #          "msg":"User not exists"
#         #        }
#         #
#         #     - GroupType không tồn tại trong hệ thống
#         #      Status Code: 400
#         #      Body:
#         #        {
#         #          "code":"0x0001",
#         #          "msg":"GroupType not exists"
#         #        }
#         #
#         #     - Dữ liệu gởi lên không đủ các trường quy định
#         #      Status Code: 400
#         #      Body:
#         #        {
#         #          "code":"0x0001",
#         #          "msg":"params require not exists"
#         #        }
#         #
#         def update
#           ActiveRecord::Base.transaction do
#             date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
#             put_params = permit_params(params)
#             # Check params
#             if put_params[:leader].blank? \
#             || put_params[:leader][:username].blank? \
#             || put_params[:name].blank?
#               render json: {
#                 code: '0x0001',
#                 msg: 'params require not exists'
#               }, status: 400 and return
#             end
#             # Update Group
#             put_params[:password] = Digest::SHA1.hexdigest(put_params[:password]) unless put_params[:password].blank?
#             user_lead_id = User.find_by!(username: put_params[:leader][:username]).id
#             group_id = Group.find_by!(user_id: user_lead_id, role_id: 4 ) TODO :DANG LAM O DAY
#             group_new = Group.find!(id: group_id)
#             group_new.update!(
#               name: put_params[:name],
#               descripton: put_params[:descripton],
#               encrypted_password: put_params[:password],
#               group_type_id: put_params[:group_type][:id],
#               created_at: date_time_now_with_time_zone,
#               updated_at: date_time_now_with_time_zone
#             )
#             unless put_params[:image].blank?
#               path = File.join(GROUP_AVATARS_PATH, group_new.id.to_s)
#               file_path = Rails.root.join('public', path)
#               File.open(file_path, 'wb') do |file|
#                 file.write(put_params[:image])
#               end
#               group_new.update!(
#                 group_avatar_url: "/#{path}"
#               )
#             end
#             # Create UserRoleGroup
#             UserRoleGroup.create!(
#               user_id: User.find_by!(username: put_params[:leader][:username]).id,
#               role_id: 4,
#               group_id: group_new.id,
#               created_at: date_time_now_with_time_zone,
#               updated_at: date_time_now_with_time_zone
#             )
#             @group = group_new
#             render rabl_path, status: 201
#           end
#         # Catch errors
#         rescue ActiveRecord::RecordNotFound => ex
#           render json: {
#             code: '0x0001',
#             msg: 'User not exists'
#           }, status: 400
#         rescue ActiveRecord::InvalidForeignKey => ex
#           render json: {
#             code: '0x0001',
#             msg: 'GroupType not exists'
#           }, status: 400
#         end

#       private
#         # Never trust parameters from the scary internet, only allow the white list through.
#         def permit_params(params)
#           params.permit(
#             :name,
#             :descripton,
#             :password,
#             :image
#           ).tap do |wl|
#             wl[:leader]     = params[:leader]
#             wl[:group_type] = params[:group_type]
#           end.delete_if{|key,value| value.blank?}
#         end
#       end
#     end
#   end
# end
