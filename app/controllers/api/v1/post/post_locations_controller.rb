# encoding: utf-8

module Api
  module V1
    module Post
      class PostLocationsController < BaseController
        # ==== Description
        #
        # Ghi nhận vị trí User
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/locations(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/locations.json
        #    body:
        #    {
        #      "member":{
        #        "username":"hien"
        #      },
        #      "longitude":"10.01",
        #      "latitude":"10.01",
        #      "usertime":"1409822681"
        #    }
        #
        # ==== Request Parameters
        #
        # * member
        #   - Description: User cần ghi nhận vị trí
        #   - Data Type: hash
        # * username
        #   - Description: username của user
        #   - Data Type: string
        # * longitude
        #   - Description: Kinh độ
        #   - Data Type: string
        # * latitude
        #   - Description: Vĩ độ
        #   - Data Type: string
        # * usertime
        #   - Description: Thời gian user ghi nhận được lúc gởi
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 khi tạo thành công
        # * *Example:*
        #   - Khi tạo thành công
        #    Status Code: 201
        #    Body:
        #      true
        #
        #   - Khi tạo không thành công:
        #     - User không tồn tại trong hệ thống
        #      Status Code: 201
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"User not exists"
        #        }
        #
        #     - Dữ liệu gởi lên bị lỗi
        #       - Dữ liệu Kinh độ không đúng format
        #        Status Code: 400
        #        Body:
        #          {
        #            "code":"0x0001",
        #            "msg":"Longitude is not a number"
        #          }
        #
        #       - Dữ liệu Vĩ độ không đúng format
        #        Status Code: 400
        #        Body:
        #          {
        #            "code":"0x0001",
        #            "msg":"Latitude is not a number"
        #          }
        #
        #       - Dữ liệu Thời gian user ghi nhận được lúc gởi không đúng format
        #        Status Code: 400
        #        Body:
        #          {
        #            "code":"0x0001",
        #            "msg":"usertime value out of range"
        #          }
        #
        #        Status Code: 400
        #        Body:
        #          {
        #            "code":"0x0001",
        #            "msg":"usertime is not a number"
        #          }
        #
        #       - Dữ liệu gởi lên không đủ các trường quy định
        #        Status Code: 400
        #        Body:
        #          {
        #            "code":"0x0001",
        #            "msg":"params require not exists"
        #          }
        #
        def create
          ActiveRecord::Base.transaction do
            date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
            post_params = permit_params(params)
            # Check params
            if post_params[:member].blank? \
            || post_params[:member][:username].blank? \
            || post_params[:usertime].blank?
              render json: {
                code: '0x0001',
                msg: 'params require not exists'
              }, status: 400 and return
            end
            # DateTime.strptime can handle seconds since epoch
            # http://stackoverflow.com/questions/7816365/how-to-convert-a-unix-timestamp-seconds-since-epoch-to-ruby-datetime
            begin
              usertime = DateTime.strptime(post_params[:usertime].to_s,'%s')
            rescue
              render json: {
                code: '0x0001',
                msg: 'usertime is not a number'
              }, status: 400 and return
            end

            user_id = User.find_by!(username: post_params[:member][:username]).id
            longitude = post_params[:longitude]
            latitude = post_params[:latitude]

            loc = Location.new(
              user_id: user_id,
              longitude: longitude,
              latitude: latitude,
              usertime: usertime,
              created_at: date_time_now_with_time_zone,
              updated_at: date_time_now_with_time_zone
            )
            if loc.valid?
              query = 'INSERT INTO locations (user_id, longitude, latitude, '\
                  'usertime, created_at, updated_at) VALUES ('\
                  "#{user_id},"\
                  "#{longitude},"\
                  "#{latitude},"\
                  "'#{usertime}',"\
                  "'#{date_time_now_with_time_zone}',"\
                  "'#{date_time_now_with_time_zone}');"

              ActiveRecord::Base.connection.execute(query)
              render json: true, status: 201
            else
              render json: {
                code: '0x0001',
                msg: loc.errors
              }, status: 400
            end
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: {
            code: '0x0001',
            msg: 'User not exists'
          }, status: 400
        rescue ActiveRecord::RecordInvalid => ex
          render json: {
            code: '0x0001',
            msg: ex.message
          }, status: 400
        rescue ActiveRecord::StatementInvalid => ex
          render json: {
            code: '0x0001',
            msg: 'usertime value out of range'
          }, status: 400
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :longitude,
            :latitude,
            :usertime
          ).tap do |wl|
            wl[:member] = params[:member]
          end.delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
