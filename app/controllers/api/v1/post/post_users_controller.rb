# encoding: utf-8

module Api
  module V1
    module Post
      class PostUsersController < ApplicationController
        include Api::V1::Commoms

        # Path of user_avatars
        USER_AVATARS_PATH = 'system/user_avatars'

        # ==== Description
        #
        # Tạo mới tài khoản
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/users(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="5286a42c3f8625a7fff962299311bf9163478e48"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/users.json
        #    body:
        #    {
        #      "email": "khoa@ersolution.net",
        #      "username": "admin",
        #      "password": "123456789",
        #      "fullname": "Nguyễn Đăng Khoa",
        #      "cell_phone": "0907 205 855",
        #      "image":"nội dung file"
        #    }
        #
        # ==== Request Parameters
        #
        # * email
        #   - Description: Địa chỉ mail NOT NULL
        #   - Data Type: string
        # * username
        #   - Description: Username NOT NULL
        #   - Data Type: string
        # * password
        #   - Description: Mật khẩu
        #   - Data Type: string
        # * fullname
        #   - Description: Tên đầy đủ
        #   - Data Type: string
        # * cell_phone
        #   - Description: Số điện thoại
        #   - Data Type: string
        # * image
        #   - Description: Nội dung file ảnh đại diện
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 và User khi tạo thành công
        # * *Example:*
        #   - Khi tạo thành công
        #    Status Code: 201
        #    Body:
        #      {
        #        "id": 1,
        #        "email": "khoa@ersolution.net",
        #        "username": "admin",
        #        "fullname": "Nguyễn Đăng Khoa",
        #        "cell_phone": null,
        #        "user_avatar_url": null,
        #        "access_token": "c3f08aa00b6a90dace29060d89f681b437709914",
        #        "expires_in": null
        #      }
        #
        #   - Khi tạo không thành công:
        #     - User không tồn tại trong hệ thống
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"User not exists"
        #        }
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def create
          date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
          post_params = permit_params(params)
          # Check params
          if post_params[:username].blank? \
          || post_params[:email].blank?
            render json: {
              code: '0x0001',
              msg: 'params require not exists'
            }, status: 400 and return
          end
          # Generate new Access Token
          access_token = SecureRandom.hex(20)
          # Create User
          user_new = User.create!(
            email: post_params[:email],
            username: post_params[:username],
            password: post_params[:password],
            fullname: post_params[:fullname],
            cell_phone: post_params[:cell_phone],
            access_token: access_token,
            created_at: date_time_now_with_time_zone,
            updated_at: date_time_now_with_time_zone
          )
          unless post_params[:image].blank?
            path = File.join(USER_AVATARS_PATH, user_new.id.to_s)
            file_path = Rails.root.join('public', path)
            File.open(file_path, 'wb') do |file|
              file.write(post_params[:image])
            end
            user_new.update!(
              user_avatar_url: "/#{path}"
            )
          end
          user_new[:expire_at] = user_new[:expire_at].to_i unless user_new[:expire_at].blank?
          @user = user_new
          render rabl_path, status: 201
        # Catch errors
        rescue ActiveRecord::RecordInvalid => ex
          render json: {
            code: '0x0001',
            msg: ex.message
          }, status: 400
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :email,
            :username,
            :password,
            :fullname,
            :cell_phone,
            :image
          )
        end
      end
    end
  end
end
