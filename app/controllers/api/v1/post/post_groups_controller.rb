# encoding: utf-8

module Api
  module V1
    module Post
      class PostGroupsController < BaseController
        # Path of group_avatars
        GROUP_AVATARS_PATH = 'system/group_avatars'

        # ==== Description
        #
        # Tạo group mới
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/groups(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="23f4b0550d33b7d733a762cda04c69fce0d00b7c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/groups.json
        #    body:
        #    {
        #      "leader":{
        #        "username":"admin"
        #      },
        #      "name":"group_name_01",
        #      "password":"123456789",
        #      "image":"nội dung file",
        #      "descripton":"mô tả group",
        #      "group_type":{
        #        "id":"1"
        #      }
        #    }
        #
        # ==== Request Parameters
        #
        # * leader
        #   - Description: User tạo group
        #   - Data Type: hash
        # * username
        #   - Description: Username của leader
        #   - Data Type: string
        # * name
        #   - Description: Tên của group
        #   - Data Type: string
        # * password
        #   - Description: Mật khẩu của group
        #   - Data Type: string
        # * image
        #   - Description: Nội dung file ảnh đại diện của group
        #   - Data Type: string
        # * descripton
        #   - Description: Mô tả group
        #   - Data Type: text
        # * group_type
        #   - Description: Loại group
        #   - Data Type: hash
        # * id
        #   - Description: id của loại group
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 và Group khi tạo thành công
        # * *Example:*
        #   - Khi tạo thành công
        #    Status Code: 201
        #    Body:
        #      {
        #        "id": 2,
        #        "name": "group_name_01",
        #        "descripton": "mô tả group",
        #        "encrypted_password": "f7c3bc1d808e04732adf679965ccc34ca7ae3441",
        #        "group_avatar_url": "/system/group_avatars/2",
        #        "group_type_id": 1
        #      }
        #
        #   - Khi tạo không thành công:
        #     - User không tồn tại trong hệ thống
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"User not exists"
        #        }
        #
        #     - GroupType không tồn tại trong hệ thống
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"GroupType not exists"
        #        }
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def create
          ActiveRecord::Base.transaction do
            date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
            post_params = permit_params(params)
            # Check params
            if post_params[:leader].blank? \
            || post_params[:leader][:username].blank? \
            || post_params[:name].blank? \
            || post_params[:group_type].blank? \
            || post_params[:group_type][:id].blank?
              render json: {
                code: '0x0001',
                msg: 'params require not exists'
              }, status: 400 and return
            end
            # Create Group
            post_params[:password] = Digest::SHA1.hexdigest(post_params[:password]) unless post_params[:password].blank?
            group_new = Group.create!(
              name: post_params[:name],
              descripton: post_params[:descripton],
              encrypted_password: post_params[:password],
              group_type_id: post_params[:group_type][:id],
              created_at: date_time_now_with_time_zone,
              updated_at: date_time_now_with_time_zone
            )
            unless post_params[:image].blank?
              path = File.join(GROUP_AVATARS_PATH, group_new.id.to_s)
              file_path = Rails.root.join('public', path)
              File.open(file_path, 'wb') do |file|
                file.write(post_params[:image])
              end
              group_new.update!(
                group_avatar_url: "/#{path}"
              )
            end
            # Create UserRoleGroup
            UserRoleGroup.create!(
              user_id: User.find_by!(username: post_params[:leader][:username]).id,
              role_id: 4,
              group_id: group_new.id,
              created_at: date_time_now_with_time_zone,
              updated_at: date_time_now_with_time_zone
            )
            @group = group_new
            render rabl_path, status: 201
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: {
            code: '0x0001',
            msg: 'User not exists'
          }, status: 400
        rescue ActiveRecord::InvalidForeignKey => ex
          render json: {
            code: '0x0001',
            msg: 'GroupType not exists'
          }, status: 400
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :name,
            :descripton,
            :password,
            :image
          ).tap do |wl|
            wl[:leader]     = params[:leader]
            wl[:group_type] = params[:group_type]
          end.delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
