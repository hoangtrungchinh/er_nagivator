# encoding: utf-8

module Api
  module V1
    module Post
      class PostUsersLogInController < ApplicationController
        include Api::V1::Commoms

        # Time for expire_at
        EXPIRE_TIME = 3.months

        # ==== Description
        #
        # Đăng nhập
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/login(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="5286a42c3f8625a7fff962299311bf9163478e48"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/login.json
        #    body:
        #    {
        #      "username": "admin",
        #      "password": "123456789"
        #    }
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Username NOT NULL
        #   - Data Type: string
        # * password
        #   - Description: Mật khẩu
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 và User khi đăng nhập thành công
        # * *Example:*
        #   - Khi đăng nhập thành công
        #    Status Code: 201
        #    Body:
        #      {
        #        "id": 1,
        #        "email": "khoa@ersolution.net",
        #        "username": "admin",
        #        "fullname": "Nguyễn Đăng Khoa",
        #        "cell_phone": null,
        #        "user_avatar_url": null,
        #        "access_token": "c3f08aa00b6a90dace29060d89f681b437709914",
        #        "expires_in": 1418017736
        #      }
        #
        #   - Khi đăng nhập không thành công:
        #     - Sai password
        #      Status Code: 401
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"Unauthenticated"
        #        }
        #
        #     - User không tồn tại trong hệ thống
        #      Status Code: 401
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"Unauthenticated"
        #        }
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def create
          date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
          post_params = permit_params(params)
          # Check params
          if post_params[:username].blank?
            render json: {
              code: '0x0001',
              msg: 'params require not exists'
            }, status: 400 and return
          end
          # Check User
          user = User.find_by!(username: post_params[:username])
          if user.valid_password?(post_params[:password])
            # Generate new Access Token and Expire time
            access_token = SecureRandom.hex(20)
            expire_at = (DateTime.now.in_time_zone + EXPIRE_TIME).strftime("%Y-%m-%dT%H:%M:%S%:z")
            user.update!(
              access_token: access_token,
              expire_at: expire_at
            )
            user[:expire_at] = user[:expire_at].to_i unless user[:expire_at].blank?
            @user = user
            render rabl_path , status: 201
          else
            # Password not accepted
            render json: {
            code: '0x0001',
            msg: 'Unauthenticated'
          }, status: 401
          end
        # # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          # User not exists
          render json: {
            code: '0x0001',
            msg: 'Unauthenticated'
          }, status: 401
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :password
          )
        end
      end
    end
  end
end
