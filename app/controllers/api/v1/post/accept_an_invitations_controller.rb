# encoding: utf-8

module Api
  module V1
    module Post
      class AcceptAnInvitationsController < BaseController
        # ==== Description
        #
        # User chấp nhận vào nhóm
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/accepted(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/accepted.json
        #    body:
        #    {
        #      "member": {
        #        "username": "hien"
        #      },
        #      "host": {
        #        "username": "admin"
        #      },
        #      "group": {
        #        "id": "1"
        #      }
        #    }
        #
        # ==== Request Parameters
        #
        # * member
        #   - Description: User muốn vào group
        #   - Data Type: hash
        # * host
        #   - Description: User mời vào group
        #   - Data Type: hash
        # * username
        #   - Description: Username của User
        #   - Data Type: string
        # * group
        #   - Description: group được mời
        #   - Data Type: hash
        # * id
        #   - Description: id của group được mời
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 khi tạo thành công
        # * *Example:*
        #   - Khi tạo thành công
        #    Status Code: 201
        #    Body:
        #      true
        #
        #   - Khi tạo không thành công:
        #     - User không tồn tại trong hệ thống
        #     - Group không tồn tại trong hệ thống
        #     - User đã tồn tại trong group
        #      Status Code: 201
        #      Body:
        #        false
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def create
          ActiveRecord::Base.transaction do
            date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
            post_params = permit_params(params)
            # Check params
            if post_params[:member].blank? \
            || post_params[:member][:username].blank? \
            || post_params[:host].blank? \
            || post_params[:host][:username].blank? \
            || post_params[:group].blank? \
            || post_params[:group][:id].blank?
              render json: {
                code: '0x0001',
                msg: 'params require not exists'
              }, status: 400 and return
            end
            group = Group.find_by!(id: post_params[:group][:id])
            member = User.find_by!(username: post_params[:member][:username])
            host = User.find_by!(username: post_params[:host][:username])
            # Create UserRoleGroup
            UserRoleGroup.create!(
              user_id: member.id,
              role_id: 5,
              group_id: group.id,
              created_at: date_time_now_with_time_zone,
              updated_at: date_time_now_with_time_zone
            )
            # Destroy InviteInfo
            invite_info = InviteInfo.find_by!(
              host_id: host.id,
              user_id: member.id,
              group_id: group.id
            )
            InviteInfo.destroy(invite_info.id)
            render json: true, status: 201
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: false, status: 201
        rescue ActiveRecord::RecordInvalid => ex
          render json: false, status: 201
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
          ).tap do |wl|
            wl[:member] = params[:member]
            wl[:host] = params[:host]
            wl[:group]  = params[:group]
          end.delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
