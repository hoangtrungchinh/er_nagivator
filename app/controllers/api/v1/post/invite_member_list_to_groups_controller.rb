# encoding: utf-8

module Api
  module V1
    module Post
      class InviteMemberListToGroupsController < BaseController
        # ==== Description
        #
        # Mời thành viên vào nhóm
        #
        # ==== Request
        #
        # * Method: POST
        # * URL: /api/invites(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="23f4b0550d33b7d733a762cda04c69fce0d00b7c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    POST /api/invites.json
        #    body:
        #    {
        #      "host": {
        #        "username": "admin"
        #      },
        #      "member_list": [
        #        {
        #          "username": "hien"
        #        },
        #        {
        #          "username": "chinh"
        #        }
        #      ],
        #      "group": {
        #        "id": "1"
        #      }
        #    }
        #
        # ==== Request Parameters
        #
        # * host
        #   - Description: User mời vào group
        #   - Data Type: hash
        # * member_list
        #   - Description: List User được mời
        #   - Data Type: array hash
        # * username
        #   - Description: Username của User
        #   - Data Type: string
        # * group
        #   - Description: Group được mời
        #   - Data Type: hash
        # * id
        #   - Description: Id của group được mời
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 201 khi tạo thành công
        # * *Example:*
        #   - Khi tạo thành công
        #    Status Code: 201
        #    Body:
        #      true
        #
        #   - Khi tạo không thành công:
        #     - User đã được mời rồi
        #      Status Code: 201
        #      Body:
        #        false
        #
        #     - User không tồn tại trong hệ thống
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"User not exists"
        #        }
        #
        #     - GroupType không tồn tại trong hệ thống
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"GroupType not exists"
        #        }
        #
        #     - Dữ liệu gởi lên có member_username trùng với host_username
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"member_username and host_username not unique"
        #        }
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def create
          ActiveRecord::Base.transaction do
            date_time_now_with_time_zone = DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
            post_params = permit_params(params)
            # Check params
            if post_params[:host].blank? \
            || post_params[:host][:username].blank? \
            || post_params[:member_list].blank? \
            || post_params[:group].blank? \
            || post_params[:group][:id].blank?
              render json: {
                code: '0x0001',
                msg: 'params require not exists'
              }, status: 400 and return
            end
            post_params[:member_list].each do |member_params|
              # Check params
              if member_params[:username].blank?
                render json: {
                  code: '0x0001',
                  msg: 'params require not exists'
                }, status: 400 and return
              end
              if member_params[:username] == post_params[:host][:username]
                render json: {
                  code: '0x0001',
                  msg: 'member_username and host_username not unique'
                }, status: 400 and return
              end
              # Check user exist in group
              post_params[:member_list].each do |member_params|
                user_id = User.find_by!(username: member_params[:username]).id
                group_id = post_params[:group][:id]
                user_in_group = UserRoleGroup.find_by(user_id:user_id, group_id:group_id)
                unless user_in_group.blank?
                  render json: false, status: 201 and return
                end
              end
              InviteInfo.create!(
                host_id: User.find_by!(username: post_params[:host][:username]).id,
                user_id: User.find_by!(username: member_params[:username]).id,
                group_id: post_params[:group][:id],
                created_at: date_time_now_with_time_zone,
                updated_at: date_time_now_with_time_zone
              )
            end
            render json: true, status: 201
          end
        # Catch errors
        rescue ActiveRecord::RecordInvalid => ex
          render json: false, status: 201
        rescue ActiveRecord::RecordNotFound => ex
          render json: {
            code: '0x0001',
            msg: 'User not exists'
          }, status: 400
        rescue ActiveRecord::InvalidForeignKey => ex
          render json: {
            code: '0x0001',
            msg: 'GroupType not exists'
          }, status: 400
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
          ).tap do |wl|
            wl[:host]        = params[:host]
            wl[:leader]      = params[:leader]
            wl[:member_list] = params[:member_list]
            wl[:group]       = params[:group]
          end.delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
