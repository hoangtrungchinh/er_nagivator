# encoding: utf-8

module Api
  module V1
    module Delete
      class DeleteInvitationsController < BaseController
        # ==== Description
        #
        # User hủy lời mời
        #
        # ==== Request
        #
        # * Method: DELETE
        # * URL: /api/invitations(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    DELETE /api/invitations.json?id=1
        #
        # ==== Request Parameters
        #
        # * id
        #   - Description: Id lời mời cần xóa
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200
        # * *Example:*
        #    Status Code: 200
        #    Body:
        #      true
        #
        def destroy
          delete_params = permit_params(params)
          InviteInfo.destroy(delete_params[:id])
          render json: true
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: true
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :id
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
