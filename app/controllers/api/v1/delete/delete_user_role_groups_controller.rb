# encoding: utf-8

module Api
  module V1
    module Delete
      class DeleteUserRoleGroupsController < BaseController
        # ==== Description
        #
        # User rời nhóm
        #
        # ==== Request
        #
        # * Method: DELETE
        # * URL: /api/user_role_groups(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    DELETE /api/user_role_groups.json?username=1&group_id=1
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Tên user muốn rời group
        #   - Data Type: string
        #
        # * group_id
        #   - Description: Id group mà user thuộc về
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200
        # * *Example:*
        #   - Khi rời nhóm thành công
        #    Status Code: 200
        #    Body:
        #      true
        #
        #   - Khi rời nhóm không thành công:
        #     - User không tồn tại trong nhóm
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"User not exists in group"
        #        }
        #
        #     - Dữ liệu gởi lên không đủ các trường quy định
        #      Status Code: 400
        #      Body:
        #        {
        #          "code":"0x0001",
        #          "msg":"params require not exists"
        #        }
        #
        def destroy
          delete_params = permit_params(params)
          # Check params
          if delete_params[:username].blank? || delete_params[:group_id].blank?
            render json: {
              code: '0x0001',
              msg: 'params require not exists'
            }, status: 400 and return
          end
          user_id = User.find_by!(username: delete_params[:username]).id
          user_role_group = UserRoleGroup.find_by!(user_id: user_id, group_id:delete_params[:group_id])
          user_role_group.destroy
          render json: true
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
            render json: {
              code: '0x0001',
              msg: 'User not exists in group'
            }, status: 400
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :group_id
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
