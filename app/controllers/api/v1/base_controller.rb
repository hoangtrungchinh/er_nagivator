# encoding: utf-8
module Api
  module V1
    class BaseController < ActionController::Base
      include Api::V1::Commoms

      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      # protect_from_forgery with: :exception
      # Rails 4 solution for "Can't verify CSRF token authenticity” json requests
      # https://coderwall.com/p/8z7z3a
      protect_from_forgery with: :null_session
      skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format.json? }

      # Secure API
      # http://blog.envylabs.com/post/75521798481/token-based-authentication-in-rails
      before_action :restrict_access

      def missing_page
        render json: 'Not Found', :status => 404
      end

      #===============================LOGGING============================================
        # Write error logs
        Log4r::Logger.root
        def error_logger(message)
          error_logger = Log4r::Logger.new('Error Logger')
          date_log = Log4r::DateFileOutputter.new('DateLog', dirname: 'log/error', filename: 'error.log')
          format = Log4r::PatternFormatter.new(pattern: "PID:%p %m", date_pattern: "%H:%M:%S")
          date_log.formatter = format
          error_logger.add(date_log)
          error_logger.info message
        end

        def write_error_logger(ex)
          #S========== ERROR LOG ==========
            error_logger(ex.inspect)
            error_logger(ex.backtrace.join("\n"))
            error_logger("\n\n\n ")
          #E========== ERROR LOG ==========
        end

        rescue_from Exception, :with => :internal_error
        def internal_error(ex)
          write_error_logger(ex)
          raise ex
        end
      #===============================LOGGING============================================

      private
        # Check access_token
        def restrict_access
          authenticate_or_request_with_http_token do |token, options|
            @user = User.find_by(access_token: token)
            if @user
              true
            else
              head 403
            end
          end
        end
    end
  end
end
