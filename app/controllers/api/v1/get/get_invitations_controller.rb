# encoding: utf-8

module Api
  module V1
    module Get
      class GetInvitationsController < BaseController
        # ==== Description
        #
        # Lấy danh sách lời mời của User
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/invitations(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/invitations.json?username=hien
        # * *Todo:*
        #    GET /api/invitations.json?username=hien&limit=3&offset=0
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Username của User cần lấy danh sách lời mời
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200 và các Object liên quan
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #    [
        #      {
        #        "id": 1,
        #        "host": {
        #          "id": 1,
        #          "email": "khoa@ersolution.net",
        #          "username": "admin",
        #          "fullname": "Nguyễn Đăng Khoa",
        #          "cell_phone": null,
        #          "user_avatar_url": null
        #        },
        #        "leader": {
        #          "id": 1,
        #          "email": "khoa@ersolution.net",
        #          "username": "admin",
        #          "fullname": "Nguyễn Đăng Khoa",
        #          "cell_phone": null,
        #          "user_avatar_url": null
        #        },
        #        "group": {
        #          "id": 1,
        #          "name": "group_name_01",
        #          "descripton": null,
        #          "encrypted_password": null,
        #          "group_avatar_url": null,
        #          "group_type": {
        #            "id": 1,
        #            "name": "OPEN_GROUP",
        #            "descripton": null
        #          }
        #        }
        #      }
        #    ]
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      []
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/invitations.json?username=chinh&filter=group.id,group.group_type.id,host.id,id
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "id": 4,
        #          "host": {
        #            "id": 1
        #          },
        #          "group": {
        #            "id": 9,
        #            "group_type": {
        #              "id": 3
        #            }
        #          }
        #        }
        #      ]
        #
        #     * *Example:*
        #      GET /api/invitations.json?username=chinh&filter=group.id,group.group_type
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "group": {
        #            "id": 9,
        #            "group_type": {
        #              "id": 3,
        #              "name": "SECRET_GROUP",
        #              "descripton": null
        #            }
        #          }
        #        }
        #      ]
        #
        #     * *Example:*
        #      GET /api/invitations.json?username=chinh&filter=id,group,host
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "id": 4,
        #          "host": {
        #            "id": 1,
        #            "email": "khoa@ersolution.net",
        #            "username": "admin",
        #            "fullname": "Nguyễn Đăng Khoa",
        #            "cell_phone": null,
        #            "user_avatar_url": null
        #          },
        #          "group": {
        #            "id": 9,
        #            "name": "group_vip_secret_02",
        #            "descripton": null,
        #            "group_avatar_url": null
        #          }
        #        }
        #      ]
        #
        #     * *Example:*
        #      GET /api/invitations.json?username=chinh&filter=id,group.group_type,leader,group.id
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "id": 4,
        #          "leader": {
        #            "id": 1,
        #            "email": "khoa@ersolution.net",
        #            "username": "admin",
        #            "fullname": "Nguyễn Đăng Khoa",
        #            "cell_phone": null,
        #            "user_avatar_url": null
        #          },
        #          "group": {
        #            "id": 9,
        #            "group_type": {
        #              "id": 3,
        #              "name": "SECRET_GROUP",
        #              "descripton": null
        #            }
        #          }
        #        }
        #      ]
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          unless get_params[:username].blank?
            @invite_infos = InviteInfo.where(
              user_id: User.find_by!(username: get_params[:username]).id
            )
          else
            render json: [], root: false
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: [], root: false
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
