# encoding: utf-8

module Api
  module V1
    module Get
      class GetGroupsController < BaseController
        # ==== Description
        #
        # Lấy thông tin nhóm và member trong nhóm theo id
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/groups(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/groups.json?id=1
        #
        # ==== Request Parameters
        #
        # * id
        #   - Description: Id group cần lấy thông tin
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200 và nội dung là tất cả thông tin của group
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #      {
        #        "id": 1,
        #        "name": "group_name_01",
        #        "descripton": null,
        #        "group_avatar_url": null,
        #        "group_type": {
        #          "id": 1,
        #          "name": "OPEN_GROUP",
        #          "descripton": null,
        #        },
        #        "members": [
        #          {
        #            "id": 1,
        #            "email": "khoa@ersolution.net",
        #            "username": "admin",
        #            "fullname": "Nguyễn Đăng Khoa",
        #            "cell_phone": null,
        #            "user_avatar_url": null
        #          }
        #        ]
        #      }
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      null
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/groups.json?id=1&filter=id,name,group_type.name
        #      Status Code: 200
        #      Body:
        #      {
        #        "id": 1,
        #        "name": "group_open_01",
        #        "group_type": {
        #          "name": "OPEN_GROUP"
        #        }
        #      }
        #
        #     * *Example:*
        #      GET /api/groups.json?id=1&filter=id,name,group_type
        #      Status Code: 200
        #      Body:
        #      {
        #        "id": 1,
        #        "name": "group_open_01",
        #        "group_type": {
        #          "id": 1,
        #          "name": "OPEN_GROUP",
        #          "descripton": null
        #        }
        #      }
        #
        #     * *Example:*
        #      GET /api/groups.json?id=1&filter=id,name,group_type,members.id
        #      Status Code: 200
        #      Body:
        #      {
        #        "id": 1,
        #        "name": "group_open_01",
        #        "group_type": {
        #          "id": 1,
        #          "name": "OPEN_GROUP",
        #          "descripton": null
        #        },
        #        "members": [
        #          {
        #            "id": 1
        #          },
        #          {
        #            "id": 2
        #          },
        #          {
        #            "id": 3
        #          }
        #        ]
        #      }
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          unless get_params[:id].blank?
            @group = Group.find_by!(id: get_params[:id])
          else
            render json: nil
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: nil
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :id,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
