# encoding: utf-8

module Api
  module V1
    module Get
      class GetLocationsController < BaseController
        # ==== Description
        #
        # Lấy vị trí của User
        # - Khi chỉ truyền username thì sẽ lấy vị trí mới nhất
        # - Khi truyền username, time_from và time_to thì sẽ lấy tất cả vị trí
        #   của User trong khoảng thời gian cần tìm
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/locations(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/locations.json?username=hien
        #    GET /api/locations.json?username=hien&time_from=1209822681&time_to=1609822681
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Username của User cần lấy vị trí
        #   - Data Type: string
        # * time_from
        #   - Description: Thời gian bắt đầu
        #   - Data Type: string
        # * time_to
        #   - Description: Thời gian kết thúc
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #      [
        #        {
        #          "longitude": 10.01,
        #          "latitude": 10.01,
        #          "usertime": 1409822681,
        #          "member": {
        #            "id": 2,
        #            "email": "hien@ersolution.net",
        #            "username": "hien",
        #            "fullname": "Lưu Thế Hiển",
        #            "cell_phone": null,
        #            "user_avatar_url": null
        #          }
        #        },
        #      ]
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      []
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/locations.json?username=hien&time_from=1209822681&time_to=1609822681&filter=member.id,member.email,longitude,usertime
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "longitude": 10.789617,
        #          "usertime": "2014-09-17T03:31:04.000Z",
        #          "member": {
        #            "id": 2,
        #            "email": "hien@ersolution.net"
        #          }
        #        },
        #        {
        #          "longitude": 10.78969,
        #          "usertime": "2014-09-17T03:31:04.000Z",
        #          "member": {
        #            "id": 2,
        #            "email": "hien@ersolution.net"
        #          }
        #        }
        #      ]
        #
        #     * *Example:*
        #      GET /api/locations.json?username=hien&filter=usertime,latitude,member
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "latitude": 106.64289,
        #          "usertime": 1410924664,
        #          "member": {
        #            "id": 2,
        #            "email": "hien@ersolution.net",
        #            "username": "hien",
        #            "fullname": "Lưu Thế Hiển",
        #            "cell_phone": null,
        #            "user_avatar_url": null
        #          }
        #        }
        #      ]
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          # Check params
          if !get_params[:username].blank? && !get_params[:time_from].blank? && !get_params[:time_to].blank?
            time_from = DateTime.strptime(get_params[:time_from].to_s,'%s').strftime("%Y-%m-%dT%H:%M:%S%:z")
            time_to = DateTime.strptime(get_params[:time_to].to_s,'%s').strftime("%Y-%m-%dT%H:%M:%S%:z")
            locations = Location.where(
              user_id: User.find_by!(username: get_params[:username]).id,
              usertime: time_from..time_to
            )
            unless locations.blank?
              @locations = locations
            else
              render json: [], root: false
            end
          elsif !get_params[:username].blank?
            location = Location.where(
              user_id: User.find_by!(username: get_params[:username]).id
            ).last
            unless location.blank?
              @locations = location
            else
              render json: [], root: false
            end
          else
            render json: [], root: false
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: [], root: false
        rescue ActiveRecord::StatementInvalid => ex
          render json: [], root: false
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :time_from,
            :time_to,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
