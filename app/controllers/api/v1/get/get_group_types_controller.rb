# encoding: utf-8

module Api
  module V1
    module Get
      class GetGroupTypesController < BaseController
        # ==== Description
        #
        # Lấy thông tin loại nhóm
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/group_types(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/group_types.json
        #
        # ==== Response
        #
        # - Trả về mã 200 và nội dung là tất cả thông tin của group_types
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #      [
        #        {
        #          "id": 1,
        #          "name": "OPEN_GROUP",
        #          "descripton": null,
        #        },
        #        {
        #          "id": 2,
        #          "name": "CLOSE_GROUP",
        #          "descripton": null,
        #        },
        #        {
        #          "id": 3,
        #          "name": "SECRET_GROUP",
        #          "descripton": null,
        #        }
        #      ]
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      []
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/group_types.json?filter=id,name
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "id": 1,
        #          "name": "OPEN_GROUP"
        #        },
        #        {
        #          "id": 2,
        #          "name": "CLOSE_GROUP"
        #        },
        #        {
        #          "id": 3,
        #          "name": "SECRET_GROUP"
        #        }
        #      ]
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          @group_types = GroupType.all
        end

        private
          # Never trust parameters from the scary internet, only allow the white list through.
          def permit_params(params)
            params.permit(
              :filter
            ).delete_if{|key,value| value.blank?}
          end
      end
    end
  end
end
