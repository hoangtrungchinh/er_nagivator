# encoding: utf-8

module Api
  module V1
    module Get
      class GetUserByUsernamesController < BaseController
        # ==== Description
        #
        # Lấy thông tin user
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/user(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/user.json?username=hien
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Tên user cần lấy thông tin
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200 và nội dung là tất cả thông tin của user
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #        {
        #          "id": 1
        #          "username": "admin"
        #          "fullname": "Nguyễn Đăng Khoa"
        #          "cell_phone": null
        #          "email": "khoa@ersolution.net"
        #          "user_avatar_url": null
        #        }
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      null
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/user_by_usernames.json?username=admin&filter=id,username
        #      Status Code: 200
        #      Body:
        #      {
        #        "id": 1,
        #        "username": "admin"
        #      }
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          unless get_params[:username].blank?
            @user = User.find_by!(username: get_params[:username])
          else
            render json: nil
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: nil
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
