# encoding: utf-8

module Api
  module V1
    module Get
      class GetUsersInGroupsController < BaseController
        # ==== Description
        #
        # Lấy thông tin users trong nhóm
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/users_in_group(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/users_in_group.json?id=1
        #
        # ==== Request Parameters
        #
        # * id
        #   - Description: Id group cần lấy thông tin user thành viên
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200 và nội dung là tất cả thông tin của group
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #       [
        #        {
        #          "id": 1
        #          "username": "admin"
        #          "fullname": "Nguyễn Đăng Khoa"
        #          "cell_phone": null
        #          "email": "khoa@ersolution.net"
        #          "user_avatar_url": null
        #        }
        #       ]
        #
        #   - Khi không có dữ liệu
        #    Status Code: 200
        #    Body:
        #      null
        #
        #   - Sử dụng tính năng filter
        #     * *Example:*
        #      GET /api/users_in_group.json?id=1&filter=id,email,user_avatar_url
        #      Status Code: 200
        #      Body:
        #      [
        #        {
        #          "id": 1,
        #          "email": "khoa@ersolution.net",
        #          "user_avatar_url": null
        #        },
        #        {
        #          "id": 2,
        #          "email": "hien@ersolution.net",
        #          "user_avatar_url": null
        #        },
        #        {
        #          "id": 3,
        #          "email": "chinh@ersolution.net",
        #          "user_avatar_url": null
        #        }
        #      ]
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          unless get_params[:id].blank?
            group = Group.find_by!(id: get_params[:id])
            @users = group.users
          else
            render json: nil
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: nil
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :id,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
