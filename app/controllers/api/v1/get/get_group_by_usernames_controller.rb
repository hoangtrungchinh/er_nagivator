# encoding: utf-8

module Api
  module V1
    module Get
      class GetGroupByUsernamesController < BaseController
        # ==== Description
        #
        # Lấy danh sách thông tin nhóm và member trong nhóm theo username
        #
        # ==== Request
        #
        # * Method: GET
        # * URL: /api/group_by_username(.:format)
        # * Headers:
        #   - Content-type: application/json
        #   - charset=UTF-8
        #   - Authorization: Token token="76e03840ec007606d4fce8affaa589111462a62c"
        #   - Accept: application/vnd.er_navigator.v1
        # * *Example:*
        #    GET /api/group_by_username.json?username=hien
        #
        # ==== Request Parameters
        #
        # * username
        #   - Description: Username của User cần lấy vị trí
        #   - Data Type: string
        #
        # ==== Response
        #
        # - Trả về mã 200 và nội dung là tất cả thông tin của group
        # * *Example:*
        #   - Khi có dữ liệu
        #    Status Code: 200
        #    Body:
        #    [
        #      {
        #        "id": 1,
        #        "name": "group_open_01",
        #        "descripton": null,
        #        "encrypted_password": null,
        #        "group_avatar_url": null,
        #        "group_type": {
        #          "id": 1,
        #          "name": "OPEN_GROUP",
        #          "descripton": null
        #        },
        #        "leader": {
        #          "id": 1,
        #          "email": "khoa@ersolution.net",
        #          "created_at": "2014-09-17T03:31:04.000Z",
        #          "updated_at": "2014-09-17T03:31:04.000Z",
        #          "username": "admin",
        #          "fullname": "Nguyễn Đăng Khoa",
        #          "cell_phone": null,
        #          "user_avatar_url": null,
        #          "access_token": "23f4b0550d33b7d733a762cda04c69fce0d00b7c",
        #          "expire_at": "2014-12-17T03:31:04.000Z"
        #        },
        #        "members": [
        #          {
        #            "id": 1,
        #            "email": "khoa@ersolution.net",
        #            "created_at": "2014-09-17T03:31:04.000Z",
        #            "updated_at": "2014-09-17T03:31:04.000Z",
        #            "username": "admin",
        #            "fullname": "Nguyễn Đăng Khoa",
        #            "cell_phone": null,
        #            "user_avatar_url": null,
        #            "access_token": "23f4b0550d33b7d733a762cda04c69fce0d00b7c",
        #            "expire_at": "2014-12-17T03:31:04.000Z"
        #          },
        #          {
        #            "id": 3,
        #            "email": "chinh@ersolution.net",
        #            "created_at": "2014-09-17T03:31:04.000Z",
        #            "updated_at": "2014-09-17T03:31:04.000Z",
        #            "username": "chinh",
        #            "fullname": "Hoàng Trung Chính",
        #            "cell_phone": null,
        #            "user_avatar_url": null,
        #            "access_token": "946793e1f74dee7fd9669a3a464c786906ba80c7",
        #            "expire_at": "2014-12-17T03:31:04.000Z"
        #          }
        #        ]
        #      }
        #    ]
        #
        def index
          get_params = permit_params(params)
          @filter = get_params[:filter]
          unless get_params[:username].blank?
            user_role_groups = UserRoleGroup.where(
              user_id: User.find_by!(username: get_params[:username]).id
            ).where.not(group_id: nil)
            @groups = []
            user_role_groups.each do |user_role_group|
              group_id = user_role_group.group_id
              @groups << Group.find_by!(id: group_id)
            end
          else
            render json: [], root: false
          end
        # Catch errors
        rescue ActiveRecord::RecordNotFound => ex
          render json: [], root: false
        end

      private
        # Never trust parameters from the scary internet, only allow the white list through.
        def permit_params(params)
          params.permit(
            :username,
            :filter
          ).delete_if{|key,value| value.blank?}
        end
      end
    end
  end
end
