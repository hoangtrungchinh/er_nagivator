collection @group_types

if @filter.blank?
  extends 'api/v1/group_type'
else
  @filter = @filter.split(',')
  attributes :id if @filter.include?('id')
  attributes :name if @filter.include?('name')
  attributes :descripton if @filter.include?('descripton')
end
