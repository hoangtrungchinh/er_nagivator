object @group

def group_att
  extends 'api/v1/group_attributes'
end

def group_type_att
  child :group_type do
    extends 'api/v1/get/get_groups/group_type_attributes'
  end
end

def members_att
  child :users => :members do
    extends 'api/v1/get/get_groups/user_attributes'
  end
end

if @filter.blank?
  group_att
  group_type_att
  members_att
else
  @filter = @filter.split(',')
  attributes :id if @filter.include?('id')
  attributes :name if @filter.include?('name')
  attributes :descripton if @filter.include?('descripton')
  attributes :group_avatar_url if @filter.include?('group_avatar_url')
  @filter.each do |filter|
    if filter.include?('group_type')
      group_type_att
      break
    end
  end
  @filter.each do |filter|
    if filter.include?('members')
      members_att
      break
    end
  end
end
