unless @filter.blank?
  if @filter.include? "members."
    attributes :id if @filter.include? "members.id"
    attributes :email if @filter.include? "members.email"
    attributes :username if @filter.include? "members.username"
    attributes :fullname if @filter.include? "members.fullname"
    attributes :cell_phone if @filter.include? "members.cell_phone"
    attributes :user_avatar_url if @filter.include? "members.user_avatar_url"
  else
    extends 'api/v1/user_attributes'
  end
else
  extends 'api/v1/user_attributes'
end
