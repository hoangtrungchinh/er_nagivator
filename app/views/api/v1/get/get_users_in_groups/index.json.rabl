collection @users

if @filter.blank?
  extends 'api/v1/user_attributes'
else
  @filter = @filter.split(',')
  attributes :id if @filter.include?('id')
  attributes :email if @filter.include?('email')
  attributes :username if @filter.include?('username')
  attributes :fullname if @filter.include?('fullname')
  attributes :cell_phone if @filter.include?('cell_phone')
  attributes :user_avatar_url if @filter.include?('user_avatar_url')
end