unless @filter.blank?
  if @filter.include? "member."
    attributes :id if @filter.include? "member.id"
    attributes :email if @filter.include? "member.email"
    attributes :username if @filter.include? "member.username"
    attributes :fullname if @filter.include? "member.fullname"
    attributes :cell_phone if @filter.include? "member.cell_phone"
    attributes :user_avatar_url if @filter.include? "member.user_avatar_url"
  else
    extends 'api/v1/user_attributes'
  end
else
  extends 'api/v1/user_attributes'
end
