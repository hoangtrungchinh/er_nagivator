collection @locations

if @filter.blank?
  extends "api/v1/location"
  child :user => :member do
    extends 'api/v1/user_attributes'
  end
else
  @filter = @filter.split(',')
  attributes :longitude if @filter.include?('longitude')
  attributes :latitude if @filter.include?('latitude')
  if @filter.include?('usertime')
    attributes :usertime
    node :usertime do |u|
      u.usertime.to_i
    end
  end
  @filter.each do |filter|
    if filter.include?('member')
      child :user => :member do
        extends 'api/v1/get/get_locations/user_attributes'
      end
      break
    end
  end
end
