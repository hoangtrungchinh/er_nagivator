unless @filter.blank?
  if @filter.include? "leader."
    attributes :id if @filter.include? "leader.id"
    attributes :email if @filter.include? "leader.email"
    attributes :username if @filter.include? "leader.username"
    attributes :fullname if @filter.include? "leader.fullname"
    attributes :cell_phone if @filter.include? "leader.cell_phone"
    attributes :user_avatar_url if @filter.include? "leader.user_avatar_url"
  else
    extends 'api/v1/user_attributes'
  end
else
  extends 'api/v1/user_attributes'
end
