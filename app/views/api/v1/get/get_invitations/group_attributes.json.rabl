def group_type_att
  child :group_type do
    extends 'api/v1/get/get_invitations/group_type_attributes'
  end
end

unless @filter.blank?
  if @filter.include? "group."
    attributes :id if @filter.include? "group.id"
    attributes :name if @filter.include? "group.name"
    attributes :descripton if @filter.include? "group.descripton"
    attributes :group_avatar_url if @filter.include? "group.group_avatar_url"
    @filter = @filter.split(',')
    @filter.each do |filter|
      if filter.include?('group_type')
        group_type_att
        break
      end
    end
  else
    extends 'api/v1/group_attributes'
    # group_type_att
  end
else
  extends 'api/v1/group_attributes'
  group_type_att
end
