collection @invite_infos

def host_att
  child :host => :host do
    extends "api/v1/get/get_invitations/host_attributes"
  end
end

def leader_att
  glue :group => :group_leader do
    glue :user_role_group_leader do
      child :leader => :leader do
        extends "api/v1/get/get_invitations/leader_attributes"
      end
    end
  end
end

def group_att
  child :group do
    extends "api/v1/get/get_invitations/group_attributes"
  end
end

if @filter.blank?
  attributes :id
  host_att
  leader_att
  group_att
else
  @filter = @filter.split(',')
  attributes :id if @filter.include?('id')
  @filter.each do |filter|
    if filter.include?('host')
      host_att
      break
    end
  end
  @filter.each do |filter|
    if filter.include?('leader')
      leader_att
      break
    end
  end
  @filter.each do |filter|
    if filter.include?('group')
      group_att
      break
    end
  end
end
