unless  @filter.blank?
  if @filter.include? "group.group_type."
    attributes :id if @filter.include? "group.group_type.id"
    attributes :name if @filter.include? "group.group_type.name"
    attributes :descripton if @filter.include? "group.group_type.descripton"
  else
    extends 'api/v1/group_type'
  end
else
  extends 'api/v1/group_type'
end
