unless @filter.blank?
  if @filter.include? "host."
    attributes :id if @filter.include? "host.id"
    attributes :email if @filter.include? "host.email"
    attributes :username if @filter.include? "host.username"
    attributes :fullname if @filter.include? "host.fullname"
    attributes :cell_phone if @filter.include? "host.cell_phone"
    attributes :user_avatar_url if @filter.include? "host.user_avatar_url"
  else
    extends 'api/v1/user_attributes'
  end
else
  extends 'api/v1/user_attributes'
end
