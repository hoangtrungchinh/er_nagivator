# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group_type do
    name "MyString"
    descripton "MyText"
    created_at "2014-09-03 15:28:05"
    updated_at "2014-09-03 15:28:05"
  end
end
