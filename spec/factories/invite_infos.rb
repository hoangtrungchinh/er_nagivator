# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invite_info do
    user nil
    group nil
    created_at "2014-08-26 17:58:18"
    updated_at "2014-08-26 17:58:18"
  end
end
