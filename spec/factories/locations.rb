# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    user 1
    longitude 10.8474855
    latitude 106.6680688
    usertime "2014-08-14 09:37:32"
    created_at DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
    updated_at DateTime.now.in_time_zone.strftime("%Y-%m-%dT%H:%M:%S%:z")
  end
end
