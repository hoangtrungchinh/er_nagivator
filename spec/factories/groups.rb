# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group do
    name "MyString"
    created_at "2014-08-14 05:31:24"
    updated_at "2014-08-14 05:31:24"
  end
end
