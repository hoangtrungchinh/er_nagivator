# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_role_group do
    user nil
    role nil
    group nil
    created_at "2014-08-20 14:59:23"
    updated_at "2014-08-20 14:59:23"
  end
end
