# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :role do
    name "MyString"
    created_at "2014-08-14 05:17:12"
    updated_at "2014-08-14 05:17:12"
  end
end
