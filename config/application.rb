require File.expand_path('../boot', __FILE__)

require 'rails/all'
#S========== LOG4R ==========
require 'log4r'
require 'log4r/yamlconfigurator'
require 'log4r/outputter/datefileoutputter'
include Log4r

  # Constant folders
  FOLDERS = [
    '/log/access',
    '/log/error',
    '/public/system/group_avatars',
    '/public/system/user_avatars'
  ]
#E========== LOG4R ==========

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ErNavigator
  class Application < Rails::Application
    # Manually set updated_at in Rails
    # http://stackoverflow.com/questions/1386490/manually-set-updated-at-in-rails
    ActiveRecord::Base.record_timestamps = false

    #S========== LOG4R ==========
      # Init folders for log4r
      FOLDERS.each do |folder|
        FileUtils.mkpath(Rails.root.to_s + folder)
      end

      log4r_config= YAML.load_file(File.join(File.dirname(__FILE__),"log4r.yml"))
      YamlConfigurator.decode_yaml(log4r_config['log4r_config'])
      config.logger = Log4r::Logger[Rails.env]
      # config.middleware.use( Oink::Middleware, :logger => config.logger)
    #E========== LOG4R ==========

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
  end
end
