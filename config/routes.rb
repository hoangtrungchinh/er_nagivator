# encoding: utf-8
require 'api_constraints'

Rails.application.routes.draw do

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      scope module: :get do
        get 'group_by_username'  => 'get_group_by_usernames#index'
        get 'users_in_group'     => 'get_users_in_groups#index'
        get 'locations'          => 'get_locations#index'
        get 'groups'             => 'get_groups#index'
        get 'group_types'        => 'get_group_types#index'
        get 'invitations'        => 'get_invitations#index'
        get 'roles'              => 'get_roles#index'
        get 'user_by_usernames'  => 'get_user_by_usernames#index'
        get 'locations_in_group' => 'get_locations_in_groups#index'
      end

      scope module: :post do
        post 'users'            => 'post_users#create'
        post 'login'            => 'post_users_log_in#create'
        post 'locations'        => 'post_locations#create'
        post 'groups'           => 'post_groups#create'
        post 'invites'          => 'invite_member_list_to_groups#create'
        post 'accepted'         => 'accept_an_invitations#create'
      end

      scope module: :put do
        put 'users'             => 'put_users#update'
      end

      scope module: :delete do
        delete 'invitations' => 'delete_invitations#destroy'
        delete 'user_role_groups' => 'delete_user_role_groups#destroy'
      end
    end
  end

  # devise_for :users

  # missing_page
  match '*path' => 'application#missing_page', via: :all
end
