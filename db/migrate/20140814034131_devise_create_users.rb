# encoding: utf-8

class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :email, unique: true, null: false
      t.string :encrypted_password, null: false

      ## Recoverable
      t.string   :reset_password_token
      # t.datetime :reset_password_sent_at
      t.column   :reset_password_sent_at, 'timestamptz'

      ## Rememberable
      # t.datetime :remember_created_at
      t.column   :remember_created_at, 'timestamptz'

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      # t.datetime :current_sign_in_at
      t.column   :current_sign_in_at, 'timestamptz'
      # t.datetime :last_sign_in_at
      t.column   :last_sign_in_at, 'timestamptz'
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      # t.timestamps
      t.column   :created_at, 'timestamptz', :null => false
      t.column   :updated_at, 'timestamptz'
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end
