# encoding: utf-8

class AddUsernameToUsers < ActiveRecord::Migration
  def up
    execute <<-SQL
      -- Add Column
      ALTER TABLE users ADD COLUMN username character varying(255);

      -- Set Column username To unique
      ALTER TABLE users ADD CONSTRAINT users_username_key UNIQUE (username);

      -- Add Index
      CREATE INDEX idx_users_on_username ON users (username);

      -- Add Comment
      COMMENT ON COLUMN users.username IS 'Username';
    SQL
  end

  def down
    execute <<-SQL
      -- Remove Column
      ALTER TABLE users DROP COLUMN username RESTRICT;
    SQL
  end
end
