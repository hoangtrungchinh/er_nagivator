# encoding: utf-8

class CreateLocations < ActiveRecord::Migration
  def up
    create_table :locations do |t|
      t.references :user, index: true, :null => false
      t.float      :longitude, :null => false
      t.float      :latitude, :null => false
      t.column     :usertime, 'timestamptz', :null => false
      t.column     :created_at, 'timestamptz', :null => false
      t.column     :updated_at, 'timestamptz'
    end

    # Edit table locations
    execute <<-SQL
      -- Add FOREIGN KEY
      ALTER TABLE locations
        ADD CONSTRAINT locations_user_id
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON UPDATE CASCADE;

      -- Add Comment
      COMMENT ON TABLE locations IS 'Thông tin locations';
      COMMENT ON COLUMN locations.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN locations.user_id IS 'Khóa ngoại: User được gán Location';
      COMMENT ON COLUMN locations.longitude IS 'Kinh độ';
      COMMENT ON COLUMN locations.latitude IS 'Vĩ độ';
      COMMENT ON COLUMN locations.usertime IS 'Thời gian user ghi nhận được lúc gởi';
      COMMENT ON COLUMN locations.created_at IS 'Ngày giờ create Thông tin location';
      COMMENT ON COLUMN locations.updated_at IS 'Ngày giờ update Thông tin location';
    SQL
  end

  def down
    # Drop table locations
    drop_table :locations
  end
end
