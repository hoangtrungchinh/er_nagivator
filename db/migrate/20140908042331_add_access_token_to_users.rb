# encoding: utf-8

class AddAccessTokenToUsers < ActiveRecord::Migration
  def up
    execute <<-SQL
      -- Add Column
      ALTER TABLE users ADD COLUMN access_token character varying(255);
      ALTER TABLE users ADD COLUMN expire_at timestamp with time zone;

      -- Set Column access_token To NOT NULL
      ALTER TABLE users ALTER COLUMN access_token SET NOT NULL;

      -- Add Index
      CREATE INDEX idx_users_on_access_token ON users (access_token);

      -- Add Comment
      COMMENT ON COLUMN users.access_token IS 'Access Token cấp cho User khi User đăng nhập';
      COMMENT ON COLUMN users.expire_at IS 'Thời gian có hiệu lực của token';
    SQL
  end

  def down
    execute <<-SQL
      -- Remove Column
      ALTER TABLE users DROP COLUMN expire_at RESTRICT;
      ALTER TABLE users DROP COLUMN access_token RESTRICT;
    SQL
  end
end
