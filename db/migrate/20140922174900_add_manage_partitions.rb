# encoding: utf-8

class AddManagePartitions < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE OR REPLACE FUNCTION manage_partitions(timestamptz) RETURNS void AS $$
      DECLARE
        -- name of the next partition rule and index
        v_partition_name    VARCHAR(255);
        v_rule_name         VARCHAR(255);
        v_index_name        VARCHAR(255);

        v_date_from         timestamptz;
        v_date_to           timestamptz;

        -- current date (if NULL, a current timestamptz is used)
        v_date              ALIAS FOR $1;
        v_current_date      timestamptz;

        -- used just for checking existence of the partitions
        v_exists            BOOLEAN;
      BEGIN
        IF (v_date IS NULL) THEN
            v_current_date := current_timestamp;
        ELSE
            v_current_date := v_date;
        END IF;

        -- create a partition for next month
        v_date_from := date_trunc('month', v_current_date + '1 month'::interval);
        v_date_to := v_date_from + '1 month';

        -- Table: locations
        v_partition_name := 'locations_' || EXTRACT(YEAR FROM v_date_from) || '_' || EXTRACT(MONTH FROM v_date_from);
        v_rule_name      := 'rule_' || v_partition_name;
        v_index_name     := 'idx_' || v_partition_name || '_user_id';

        SELECT COUNT(*) = 1 INTO v_exists FROM pg_tables WHERE tablename = v_partition_name;

        IF (NOT v_exists) THEN
          EXECUTE 'CREATE TABLE ' || v_partition_name || ' (PRIMARY KEY (id), FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE, CHECK (usertime >= ''' || v_date_from || ''' AND usertime < ''' || v_date_to || ''')) INHERITS (locations);';
          EXECUTE 'CREATE RULE ' || v_rule_name || ' AS ON INSERT TO locations WHERE (NEW.usertime >= ''' || v_date_from || ''' AND NEW.usertime < ''' || v_date_to || ''') DO INSTEAD INSERT INTO ' || v_partition_name || ' VALUES (NEW.*);';
          EXECUTE 'CREATE INDEX ' || v_index_name || ' ON ' || v_partition_name || ' (user_id);';
        END IF;
      END;
      $$ LANGUAGE plpgsql;
      SELECT manage_partitions(current_timestamp - '1 month'::interval);
      -- SELECT manage_partitions('2014-08-22T17:42:09.857+07:00');
    SQL
  end

  def down
    execute <<-SQL
      DROP FUNCTION manage_partitions(timestamptz);
    SQL
  end
end

