# encoding: utf-8

class AddUserattrsToUsers < ActiveRecord::Migration
  def up
    execute <<-SQL
      -- Add Column
      ALTER TABLE users ADD COLUMN fullname character varying(255);
      ALTER TABLE users ADD COLUMN cell_phone character varying(255);
      ALTER TABLE users ADD COLUMN user_avatar_url character varying(255);

      -- Add Comment
      COMMENT ON COLUMN users.fullname IS 'Tên đầy đủ';
      COMMENT ON COLUMN users.cell_phone IS 'Số điện thoại';
      COMMENT ON COLUMN users.user_avatar_url IS 'Url ảnh đại diện';
    SQL
  end

  def down
    execute <<-SQL
      -- Remove Column
      ALTER TABLE users DROP COLUMN user_avatar_url RESTRICT;
      ALTER TABLE users DROP COLUMN cell_phone RESTRICT;
      ALTER TABLE users DROP COLUMN fullname RESTRICT;
    SQL
  end
end

