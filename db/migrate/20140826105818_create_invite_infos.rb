# encoding: utf-8

class CreateInviteInfos < ActiveRecord::Migration

def up
    # Create table invite_infos
    execute <<-SQL
      CREATE TABLE invite_infos -- Quản lý lời mời
      (
        id serial, -- Khóa chính tự tăng
        host_id integer NOT NULL, -- Khóa ngoại: User mời
        user_id integer NOT NULL, -- Khóa ngoại: User được mời
        group_id integer NOT NULL, -- Khóa ngoại: Group được mời
        created_at timestamp with time zone, -- Ngày giờ create Thông tin invite_info
        updated_at timestamp with time zone, -- Ngày giờ update Thông tin invite_info
        PRIMARY KEY (id),
        FOREIGN KEY (host_id) REFERENCES users(id) ON UPDATE CASCADE,
        FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE,
        FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE
      );

      -- Add constraint UNIQUE
      ALTER TABLE invite_infos ADD CONSTRAINT invite_infos_host_id_user_id_group_id_key UNIQUE (host_id,user_id,group_id);

      -- Add Index
      CREATE INDEX idx_invite_infos_host_id_user_id_group_id ON invite_infos(host_id,user_id,group_id);
      CREATE INDEX idx_invite_infos_host_id ON invite_infos(host_id);
      CREATE INDEX idx_invite_infos_user_id ON invite_infos(user_id);
      CREATE INDEX idx_invite_infos_group_id ON invite_infos(group_id);

      -- Add Comment
      COMMENT ON TABLE invite_infos IS 'Quản lý lời mời';
      COMMENT ON COLUMN invite_infos.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN invite_infos.host_id IS 'Khóa ngoại: User mời';
      COMMENT ON COLUMN invite_infos.user_id IS 'Khóa ngoại: User được mời';
      COMMENT ON COLUMN invite_infos.group_id IS 'Khóa ngoại: Group được mời';
      COMMENT ON COLUMN invite_infos.created_at IS 'Ngày giờ create Thông tin invite_info';
      COMMENT ON COLUMN invite_infos.updated_at IS 'Ngày giờ update Thông tin invite_info';
    SQL
  end

  def down
    # Drop table invite_infos
    drop_table :invite_infos
  end
end
