# encoding: utf-8

class CreateRoles < ActiveRecord::Migration
  def up
    # Create table roles
    create_table :roles do |t|
      t.string   :name, :null => false
      t.column   :created_at, 'timestamptz', :null => false
      t.column   :updated_at, 'timestamptz'
    end

    # Edit table roles
    execute <<-SQL
      -- Set Column name To unique
      ALTER TABLE roles ADD CONSTRAINT roles_name_key UNIQUE (name);

      -- Add Index
      CREATE INDEX idx_roles_on_name ON roles (name);

      -- Add Comment
      COMMENT ON TABLE roles IS 'Thông tin roles';
      COMMENT ON COLUMN roles.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN roles.name IS 'Tên role: super_admin, admin, user, leader, member';
      COMMENT ON COLUMN roles.created_at IS 'Ngày giờ create Thông tin role';
      COMMENT ON COLUMN roles.updated_at IS 'Ngày giờ update Thông tin role';
    SQL
  end

  def down
    # Drop table roles
    drop_table :roles
  end
end

