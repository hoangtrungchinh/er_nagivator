# encoding: utf-8

  # + id: integer
  # + name: string
  # + descripton: text

class CreateGroupTypes < ActiveRecord::Migration
  def up
    # Create table group_types
    create_table :group_types do |t|
      t.string   :name, unique: true, index: true, :null => false
      t.text     :descripton
      t.column   :created_at, 'timestamptz', :null => false
      t.column   :updated_at, 'timestamptz'
    end

    # Edit table group_types
    execute <<-SQL
      -- Add Comment
      COMMENT ON TABLE group_types IS 'Loại group';
      COMMENT ON COLUMN group_types.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN group_types.name IS 'Tên loại group: OPEN_GROUP, CLOSE_GROUP, SECRET_GROUP';
      COMMENT ON COLUMN group_types.descripton IS 'Mô tả loại group';
      COMMENT ON COLUMN group_types.created_at IS 'Ngày giờ create Thông tin group_type';
      COMMENT ON COLUMN group_types.updated_at IS 'Ngày giờ update Thông tin group_type';
    SQL
  end

  def down
    # Drop table group_types
    drop_table :group_types
  end
end

