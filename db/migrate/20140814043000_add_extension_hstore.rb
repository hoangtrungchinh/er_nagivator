# encoding: utf-8

class AddExtensionHstore < ActiveRecord::Migration
  def up
    # Create extension
    execute <<-SQL
      CREATE EXTENSION hstore SCHEMA pg_catalog;
    SQL
  end

  def down
    # Drop extension
    execute <<-SQL
      DROP EXTENSION hstore;
    SQL
  end
end
