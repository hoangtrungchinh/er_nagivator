# encoding: utf-8

class CreateGroups < ActiveRecord::Migration
  def up
    # Create table groups
    create_table :groups do |t|
      t.string     :name, :null => false
      t.text       :descripton
      t.string     :encrypted_password
      t.string     :group_avatar_url
      t.references :group_type, index: true, :null => false
      t.column     :created_at, 'timestamptz', :null => false
      t.column     :updated_at, 'timestamptz'
    end

    # Edit table groups
    execute <<-SQL
      -- Add FOREIGN KEY
      ALTER TABLE groups
        ADD CONSTRAINT groups_group_type_id
        FOREIGN KEY (group_type_id)
        REFERENCES group_types (id)
        ON UPDATE CASCADE;

      -- Add Index
      CREATE INDEX idx_groups_on_name ON groups (name);
      CREATE INDEX idx_groups_on_group_type_id ON groups (group_type_id);

      -- Add Comment
      COMMENT ON TABLE groups IS 'Thông tin groups';
      COMMENT ON COLUMN groups.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN groups.name IS 'Tên group';
      COMMENT ON COLUMN groups.descripton IS 'Mô tả group';
      COMMENT ON COLUMN groups.encrypted_password IS 'Mật khẩu đã được mã hóa của group';
      COMMENT ON COLUMN groups.group_avatar_url IS 'Url ảnh đại diện của group ';
      COMMENT ON COLUMN groups.group_type_id IS 'Khóa ngoại: group_type được gán cho group';
      COMMENT ON COLUMN groups.created_at IS 'Ngày giờ create Thông tin group';
      COMMENT ON COLUMN groups.updated_at IS 'Ngày giờ update Thông tin group';
    SQL
  end

  def down
    # Drop table groups
    drop_table :groups
  end
end

