# encoding: utf-8

class CreateUserRoleGroups < ActiveRecord::Migration
  def up
    create_table :user_role_groups do |t|
      t.references :user, :null => false, index: true
      t.references :role, :null => false, index: true
      t.references :group, index: true
      t.column     :created_at, 'timestamptz', :null => false
      t.column     :updated_at, 'timestamptz'
    end

    # Edit table user_role_groups
    execute <<-SQL
      -- Add FOREIGN KEY
      ALTER TABLE user_role_groups
        ADD CONSTRAINT user_role_groups_user_id
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON UPDATE CASCADE;
      ALTER TABLE user_role_groups
        ADD CONSTRAINT user_role_groups_role_id
        FOREIGN KEY (role_id)
        REFERENCES roles (id)
        ON UPDATE CASCADE;
      ALTER TABLE user_role_groups
        ADD CONSTRAINT user_role_groups_group_id
        FOREIGN KEY (group_id)
        REFERENCES groups (id)
        ON UPDATE CASCADE;

      ALTER TABLE user_role_groups
        ADD CONSTRAINT user_role_groups_unique_user_role_group
        UNIQUE(user_id, role_id, group_id);

      -- Add Comment
      COMMENT ON TABLE user_role_groups IS 'Gán Role, Group cho User';
      COMMENT ON COLUMN user_role_groups.id IS 'Khóa chính tự tăng';
      COMMENT ON COLUMN user_role_groups.user_id IS 'Khóa ngoại: User được gán Role, Group';
      COMMENT ON COLUMN user_role_groups.role_id IS 'Khóa ngoại: Role được gán cho User';
      COMMENT ON COLUMN user_role_groups.group_id IS 'Khóa ngoại: Group được gán cho User';
      COMMENT ON COLUMN user_role_groups.created_at IS 'Ngày giờ create Thông tin user_role_group';
      COMMENT ON COLUMN user_role_groups.updated_at IS 'Ngày giờ update Thông tin user_role_group';
    SQL
  end

  def down
    # Drop table user_role_groups
    drop_table :user_role_groups
  end
end
