# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140922174900) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "group_types", force: true do |t|
    t.string   "name",       null: false
    t.text     "descripton"
    t.datetime "created_at", null: false
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name",               null: false
    t.text     "descripton"
    t.string   "encrypted_password"
    t.string   "group_avatar_url"
    t.integer  "group_type_id",      null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at"
  end

  add_index "groups", ["group_type_id"], name: "idx_groups_on_group_type_id", using: :btree
  add_index "groups", ["group_type_id"], name: "index_groups_on_group_type_id", using: :btree
  add_index "groups", ["name"], name: "idx_groups_on_name", using: :btree

  create_table "invite_infos", force: true do |t|
    t.integer  "host_id",    null: false
    t.integer  "user_id",    null: false
    t.integer  "group_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invite_infos", ["group_id"], name: "idx_invite_infos_group_id", using: :btree
  add_index "invite_infos", ["host_id", "user_id", "group_id"], name: "idx_invite_infos_host_id_user_id_group_id", using: :btree
  add_index "invite_infos", ["host_id", "user_id", "group_id"], name: "invite_infos_host_id_user_id_group_id_key", unique: true, using: :btree
  add_index "invite_infos", ["host_id"], name: "idx_invite_infos_host_id", using: :btree
  add_index "invite_infos", ["user_id"], name: "idx_invite_infos_user_id", using: :btree

  create_table "locations", force: true do |t|
    t.integer  "user_id",    null: false
    t.float    "longitude",  null: false
    t.float    "latitude",   null: false
    t.datetime "usertime",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at"
  end

  add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

  create_table "locations_2014_9", force: true do |t|
    t.integer  "user_id",    null: false
    t.float    "longitude",  null: false
    t.float    "latitude",   null: false
    t.datetime "usertime",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at"
  end

  add_index "locations_2014_9", ["user_id"], name: "idx_locations_2014_9_user_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at"
  end

  add_index "roles", ["name"], name: "idx_roles_on_name", using: :btree
  add_index "roles", ["name"], name: "roles_name_key", unique: true, using: :btree

  create_table "user_role_groups", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "role_id",    null: false
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at"
  end

  add_index "user_role_groups", ["group_id"], name: "index_user_role_groups_on_group_id", using: :btree
  add_index "user_role_groups", ["role_id"], name: "index_user_role_groups_on_role_id", using: :btree
  add_index "user_role_groups", ["user_id", "group_id", "role_id"], name: "user_role_groups_unique_user_group", unique: true, using: :btree
  add_index "user_role_groups", ["user_id"], name: "index_user_role_groups_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                              null: false
    t.string   "encrypted_password",                 null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at"
    t.string   "username"
    t.string   "fullname"
    t.string   "cell_phone"
    t.string   "user_avatar_url"
    t.string   "access_token",                       null: false
    t.datetime "expire_at"
  end

  add_index "users", ["access_token"], name: "idx_users_on_access_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "idx_users_on_username", using: :btree
  add_index "users", ["username"], name: "users_username_key", unique: true, using: :btree

end
